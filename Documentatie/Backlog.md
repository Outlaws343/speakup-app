Contact Informatie
==================

Contactgegevens:

##### Naam: Stijn van der hoeven

##### Functie: Notulist

##### E-mail: <466361@student.saxion.nl>

##### Naam: Ritse Huiskes

##### Email: <465083@student.saxion.nl>

##### Naam: Volkan Sen

##### Functie: Scrum master periode 2

##### Email: <456785@student.saxion.nl>

##### Naam: Robin Persijn

##### Functie: Scrum master periode 1

##### Email: <418538@student.saxion.nl>

##### Naam: Floris Smit

##### Functie: Contact Persoon

##### Email: <464337@student.saxion.nl>

Inhoudsopgave

Inleiding
=========

“Speakup verbindt mensen met technologie zodat zij de vrijheid hebben om te
kiezen waar, wanneer en hoe zij veilig communiceren met elkaar en de wereld om
zich heen”

Met dit in gedachten gaan wij een app ontwikkelen voor Speakup die ervoor zorgt
dat cliënten met een mobiel abonnement van dit bedrijf een inzicht kunnen
krijgen in hun data verbruik. Onder data worden belminuten, sms en dataverkeer
verstaan. De opdracht bestaat eigenlijk uit vier fases die we in dit plan van
aanpak verder gaan uitwerken.

De klanten van Speakup zijn overal en altijd bereikbaar, daarom is het
belangrijk dat ze altijd en overal een inzicht hebben in de data die zij
gebruiken/verbruiken.

In dit plan van aanpak wordt in kaart gebracht hoe de realisatie zich gaat
bevinden.

Requirements
============

Functioneel

-   De mogelijkheid om een gebruiker te verifiëren.

-   De gebruiker moet zijn bel, sms en data limieten kunnen inzien.

-   De gebruiker moet zijn bel sessies kunnen inzien.

-   De gebruiker moet zijn sms gegevens kunnen inzien.

-   De gebruiker moet kunnen zien waaraan hij zijn data verbruikt heeft

-   De gebruiker moet zijn maandelijkse gebruikers overzicht kunnen inzien.

-   De app moet beschikken over contactinformatie van Speakup.

Non functioneel

-   De app moet compatibel zijn met 60% van de telefoons

-   De app moet op alle telefoons snel werken.

-   De app moet 24 uur per dag beschikbaar zijn.

-   De app moet niet de performance van de telefoon beïnvloeden wanneer deze op
    de achtergrond afspeeld.

-   De app moet in java geschreven worden.

Use Case(s)
===========

[./media/image1.png](./media/image1.png)
========================================

Mockup
======

[./media/image2.png](./media/image2.png)
========================================

![](media/97810f2e3cbf9d753b24d7a47dea0058.png)

![](media/e1a7e9d0d34b9baab0e0938a2aade548.png)

![](media/573d459a837c538cb12b3dfd256a5e70.png)

![](media/2fbb25c2327b2ec30245ed1e741c9c6c.png)

Planning

Voor het project ‘persistent’ gaan we een android applicatie maken voor het
telecommunicatiebedrijf Speak up. Om ervoor te zorgen dat dit project vlekkeloos
verloopt gaan we de eerste twee weken besteden aan het beter leren kennen van
het bedrijf en hier een planning voor te maken. Hierdoor krijgen we een beter
idee wat de klant wilt en hoe we dit kunnen visualiseren.  
In de eerste sprint gaan we bezig met het programmeren van de android
applicatie. Bij het maken van deze applicatie gaan we gebruik maken van de
planning die we hebben gemaakt. Dit is natuurlijk een richtlijn maar geeft wel
weer wat de prioriteiten zijn binnen dit project.  
  
We beginnen in sprint één met het maken van de repository op git. Hier zullen we
onze versies van de applicatie gaan beheren, en de issues in kaart brengen zodat
wij doelgericht te werk kunnen. Als we eenmaal de git up en running hebben
kunnen we bezig gaan met het eerste deel van het

programmeren namelijk het maken van de interface waar de gebruiker kan inloggen.
Tijdens de tweede sprint gaan we ons focussen op het representeren/visualiseren
van de data die voor de gebruiker van belang is.

| Activiteit                                            | Prioriteit | sprint |
|-------------------------------------------------------|------------|--------|
| Maak een interface voor de gebruiker om in te loggen. | hoog       | 1      |
| Aanmaken git repository                               | hoog       | 1      |
| Gebruikers kunnen verifiëren                          | hoog       | 1      |
| Data in/uitlezen CDR/JSON files                       | hoog       | 1/2    |
| Maak een UX die data representeert                    | middel     | 2      |
| Contact pagina maken                                  | laag       | 2      |

Bibliografie
============

**Het huidige document heeft geen bronnen.**