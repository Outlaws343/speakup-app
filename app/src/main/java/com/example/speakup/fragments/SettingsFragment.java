package com.example.speakup.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.speakup.LoginActivity;
import com.example.speakup.R;
import com.example.speakup.datamanager.DataManager;

public class SettingsFragment extends Fragment {

    DataManager dataManager = LoginActivity.DataManager;

    private Button voiceButton;
    private Button smsButton;
    private Button dataButton;

    private EditText voiceet;
    private EditText smset;
    private EditText dataet;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_settings, container, false);

        voiceButton = layout.findViewById(R.id.voiceButton);
        smsButton = layout.findViewById(R.id.smsButton);
        dataButton = layout.findViewById(R.id.dataButton);

        voiceet = layout.findViewById(R.id.voiceet);
        smset = layout.findViewById(R.id.smset);
        dataet = layout.findViewById(R.id.dataet);

        voiceet.setText("" + dataManager.getVoiceLimit());
        smset.setText("" + dataManager.getSmsLimit());
        dataet.setText("" + dataManager.getDataLimit());

        onClickToButtons();

        return layout;

    }

    public void onClickToButtons() {

        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = voiceet.getText().toString();
                if (TextUtils.isEmpty(value)) {
                    Toast.makeText(getContext(), "Please enter a valid input", Toast.LENGTH_SHORT).show();
                } else {
                    dataManager.setVoiceLimit(Double.parseDouble(value));
                    Toast.makeText(getContext(), "Saved", Toast.LENGTH_SHORT).show();
                }
            }
        });

        smsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = smset.getText().toString();
                if (TextUtils.isEmpty(value)) {
                    Toast.makeText(getContext(), "Please enter a valid input", Toast.LENGTH_SHORT).show();
                } else {
                    dataManager.setSmsLimit(Double.parseDouble(value));
                    Toast.makeText(getContext(), "Saved", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = dataet.getText().toString();
                if (TextUtils.isEmpty(value)) {
                    Toast.makeText(getContext(), "Please enter a valid input", Toast.LENGTH_SHORT).show();
                } else {
                    dataManager.setDataLimit(Double.parseDouble(value));
                    Toast.makeText(getContext(), "Saved", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}