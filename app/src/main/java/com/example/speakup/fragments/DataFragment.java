package com.example.speakup.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.speakup.LoginActivity;
import com.example.speakup.R;
import com.example.speakup.components.DataAdapter;
import com.example.speakup.components.VoiceAdapter;
import com.example.speakup.datamanager.DataManager;

public class DataFragment extends Fragment {

    RecyclerView rv;
    private RecyclerView.LayoutManager layoutManager;
    private DataManager dataManager = LoginActivity.DataManager;
    private DataAdapter dataAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_data, container, false);

        rv = layout.findViewById(R.id.rvData);
        layoutManager = new LinearLayoutManager(layout.getContext());
        rv.setLayoutManager(layoutManager);

        System.out.println("Lijst grootte" + dataManager.getListOfData().size());
        dataAdapter = new DataAdapter(dataManager.getListOfData());
        rv.setAdapter(dataAdapter);

        return layout;
    }
}
