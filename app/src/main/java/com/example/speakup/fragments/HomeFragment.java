package com.example.speakup.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.speakup.LoginActivity;
import com.example.speakup.R;
import com.example.speakup.datamanager.DataManager;
import com.example.speakup.models.Data;
import com.example.speakup.models.Sms;
import com.example.speakup.models.Voice;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private static final int ORANGE = 0xFFFF7D00;
    private static final int GREY = 0xFF75787b;

    private DataManager dataManager = LoginActivity.DataManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_home, container, false);

        voiceView(layout);
        smsView(layout);
        dataView(layout);

        return layout;
    }

    public void voiceView(View layout) {
        int duration = 0;
        ArrayList<Voice> voiceList = dataManager.getListOfVoice();

        for (int i = 0; i < voiceList.size(); i++) {
            duration += Integer.parseInt(voiceList.get(i).getDuration());
        }

        duration = (duration / 60);

        PieChart chart = layout.findViewById(R.id.voicePieChart);
        chart.getDescription().setEnabled(false);
        chart.setCenterText("Voice bundle\nin minutes");
        chart.setCenterTextColor(ORANGE);
        chart.setCenterTextSize(24f);
        chart.animateY(1200, Easing.EaseInOutQuad);
        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(62f);

        chart.setMaxAngle(270f);
        chart.setRotationAngle(135f);
        //Text below the chart
        //chart.setCenterTextOffset(0, 120);

        chart.setEntryLabelTextSize(16f);
        chart.setDrawEntryLabels(false);

        chart.setRotationEnabled(false);
        chart.setHighlightPerTapEnabled(false);

        List<PieEntry> entries = new ArrayList<>();
        ArrayList<Integer> colors = new ArrayList<>();

        if (duration != 0) {
            entries.add(new PieEntry(duration, "Usage"));
            colors.add(ORANGE);
        }

        float limit = (float) dataManager.getVoiceLimit();
        float usable = limit - duration;
        if (usable >= 0) {
            entries.add(new PieEntry(usable, "Usable"));
            colors.add(GREY);
        }

        PieDataSet dataSet = new PieDataSet(entries, "Bundle");
        dataSet.setValueTextSize(15f);
        dataSet.setSliceSpace(3f);

        dataSet.setColors(colors);

        PieData pieData = new PieData(dataSet);

        chart.setData(pieData);
        chart.invalidate();
    }

    public void smsView(View layout) {
        PieChart chart = layout.findViewById(R.id.smsPieChart);
        chart.getDescription().setEnabled(false);
        chart.setCenterText("SMS bundle\nin amount");
        chart.setCenterTextColor(ORANGE);
        chart.setCenterTextSize(24f);
        chart.animateY(1200, Easing.EaseInOutQuad);
        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(62f);

        chart.setMaxAngle(270f);
        chart.setRotationAngle(135f);
        //Text below the chart
        //chart.setCenterTextOffset(0, 120);

        chart.setEntryLabelTextSize(16f);
        chart.setDrawEntryLabels(false);

        chart.setRotationEnabled(false);
        chart.setHighlightPerTapEnabled(false);

        List<PieEntry> entries = new ArrayList<>();
        ArrayList<Integer> colors = new ArrayList<>();

        float limit = (float) dataManager.getSmsLimit();

        int usage = 0;
        for (Sms sms : dataManager.getListOfSms()){
            usage += sms.getUsage();
        }

        if (usage != 0) {
            entries.add(new PieEntry(usage, "Usage"));
            colors.add(ORANGE);
        }

        float usable = limit - usage;
        if (usable >= 0) {
            entries.add(new PieEntry(usable, "Usable"));
            colors.add(GREY);
        }

        PieDataSet dataSet = new PieDataSet(entries, "Bundle");
        dataSet.setValueTextSize(15f);
        dataSet.setSliceSpace(3f);

        dataSet.setColors(colors);

        PieData pieData = new PieData(dataSet);

        chart.setData(pieData);
        chart.invalidate();
    }

    public void dataView(View layout) {
        PieChart chart = layout.findViewById(R.id.dataPieChart);
        chart.getDescription().setEnabled(false);
        chart.setCenterText("Data bundle\nin MB's");
        chart.setCenterTextColor(ORANGE);
        chart.setCenterTextSize(24f);
        chart.animateY(1200, Easing.EaseInOutQuad);
        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(62f);

        chart.setMaxAngle(270f);
        chart.setRotationAngle(135f);
        //Text below the chart
        //chart.setCenterTextOffset(0, 120);

        chart.setEntryLabelTextSize(16f);
        chart.setDrawEntryLabels(false);

        chart.setRotationEnabled(false);
        chart.setHighlightPerTapEnabled(false);

        List<PieEntry> entries = new ArrayList<>();
        ArrayList<Integer> colors = new ArrayList<>();

        int usage = 0;
        for (Data data : dataManager.getListOfData()){
            usage += round((float) data.getUsage() / 1024, 2);
        }

        float limit = (float) dataManager.getDataLimit();

        if (usage != 0) {
            entries.add(new PieEntry(usage, "Usage"));
            colors.add(ORANGE);
        }

        float usable = limit - usage;
        if (usable >= 0 ) {
            entries.add(new PieEntry(usable, "Usable"));
            colors.add(GREY);
        }

        PieDataSet dataSet = new PieDataSet(entries, "Bundle");
        dataSet.setValueTextSize(15f);
        dataSet.setSliceSpace(3f);

        dataSet.setColors(colors);

        PieData pieData = new PieData(dataSet);

        chart.setData(pieData);
        chart.invalidate();
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
