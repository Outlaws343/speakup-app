package com.example.speakup.models;

public class DateTime {
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int second;

    public DateTime(String dateTime) {
        String[] split = dateTime.split(" ");
        String[] date = split[0].split("-");
//        String[] time = split[1].split(":");

        this.year = this.parse(date[2]);
        this.month =this.parse(date[1]);
        this.day = this.parse(date[0]);
//        this.hour = this.parse(time[0]);
//        this.minute = this.parse(time[1]);
//        this.second = this.parse(time[2]);
    }

    public int getYear() {
        return this.year;
    }

    public int getMonth() {
        return this.month;
    }

    public int getDay() {
        return this.day;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    public int getSecond() {
        return this.second;
    }

    private int parse(String value) {
        return Integer.parseInt(value);
    }
}
