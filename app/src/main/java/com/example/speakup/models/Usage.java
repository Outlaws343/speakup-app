package com.example.speakup.models;
import java.time.LocalDateTime;

public class Usage {

    // Unieke String (id?)
    private String cid;

    // Start tijd van bell/sms of data-sessie.
    private LocalDateTime time;


    private enum type {
        DATA,
        SMS,
        VOICE
    }

    // Alleen bij Voice:
    // Inkomende call: het nummer van de persoon die belde.
    // Uitgaande call: het nummer van onze mobiele klant.
    private int cli;

    // Inkomende call: het nummer van de persoon die belde.
    // Uitgaande call: het nummer van onze mobiele klant.
    // Bij data en sms: het nummer van onze mobiele klant.
    private int from;


    // Bij een uitgaande call: het nummer van de persoon die belde, dus niet het nr van onze mobiele klant.
    // Bij een inkomende call: het nummer van onze mobiele klant.
    private int to;


    // alfanumeriek (a-z 0-9) , altijd lowercase.
    private String account;


    // cause opgenomen?
    private int code;
    private String cause;

    // Voice: in seconden.
    // SMS: berichten (200 tekens in 1 bericht → 2)
    // Data: in kiloByte
    private int usage;

    // kosten voor deze CDR-regel, indien binnen bundel: 0. numeriek, 50 cent gepresenteerd als 0.5. altijd Ex BTW
    private double costs;


    // locatie waarvan wordt gebeld
    private String source;

    // locatie waarvan wordt gebeld
    private String destination;

    // incoming outgoing call
    private enum dir {
        in,
        out
    }


    // past alleen aan bij bellen naar buiteland, voicemail en doorschakelen.
    private int leg;
    // waarom leg veranderd.
    private String reason;

    // bundel naam
    private String packageName;

    // kosten buiten de bundel
    private double packageCosts;
    private double network_costs;
    private double service_costs;

}
