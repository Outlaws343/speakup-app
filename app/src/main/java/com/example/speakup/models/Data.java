package com.example.speakup.models;

import org.json.JSONException;
import org.json.JSONObject;

public class Data {

    // unique String
    private String cid;

    private String time;

    // Nummer van onze klant
    private int from;

    // alfanumeriek (a-z 0-9) , altijd lowercase.
    private String account;

    private int code;
    private String cause;

    // kilobyte
    private int usage;

    private double costs;

    private String source;

    private String destination;

    private String dir;

    private String package_name;

    private double package_costs;

    private double network_costs;

    private double service_costs;


    public Data(JSONObject jsonObject) {
        try {
            this.cid = jsonObject.getString("cid");
            this.time = jsonObject.getString("time");
            this.from = jsonObject.getInt("from");
            this.account = jsonObject.getString("account");
            this.code = jsonObject.getInt("code");
            this.cause = jsonObject.getString("cause");
            this.usage = jsonObject.getInt("usage");

            if (jsonObject.getString("costs").length() != 0) {
                this.costs = jsonObject.getDouble("costs");
            }

            this.source = jsonObject.getString("source");
            this.destination = jsonObject.getString("destination");
            this.dir = jsonObject.getString("dir");
            this.package_name = jsonObject.getString("package");


            if (jsonObject.getString("network_costs").length() != 0) {
                this.network_costs = jsonObject.getDouble("network_costs");
            }

            if (jsonObject.getString("service_costs").length() != 0) {
                this.service_costs = jsonObject.getDouble("service_costs");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getUsage() {
        return usage;
    }

    public String getTime(){
        return time;
    }
}
