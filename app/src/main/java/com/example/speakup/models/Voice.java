package com.example.speakup.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;

public class Voice {

    // unique String
    private String cid;

    // start time of call
    private LocalDateTime dateTime;

    private String duration;

    private String targetNumber;

    private String dateTimeString;


    private int from;

    // alfanumeriek (a-z 0-9) , altijd lowercase.
    private String account;

    private int code;
    private String cause;

    // In seconds
    private int usage;

    private double costs;

    private String source;

    private String destination;

    private String dir;

    private String package_name;

    private double package_costs;

    private double network_costs;

    private double service_costs;

    public Voice(JSONObject jsonObject) {
        try {


            this.cid = jsonObject.getString("cid");

            this.dateTimeString = jsonObject.getString("time");
            DateTime d = new DateTime(dateTimeString);
//            dateTime = LocalDateTime.of(d.getYear())



            this.dir = jsonObject.getString("dir");

            String toNumber = jsonObject.getString("to");
            if (dir.equals("in")) {
                this.targetNumber = jsonObject.getString("from");
                if (toNumber.contains("*")){
                    this.targetNumber = "anonymous";
                } else if (toNumber.equals("Voicemail")) {
                    this.targetNumber = jsonObject.getString("cli");
                } else {
                    this.targetNumber = toNumber;
                }
            } else {
//                System.out.println(toNumber);
                if (toNumber.contains("*")){
                    this.targetNumber = "  Anonymous";
                } else if (toNumber.equals("Voicemail")) {
                    this.targetNumber = jsonObject.getString("cli");
                } else {
                    this.targetNumber = toNumber;
                }

            }

            this.cid = jsonObject.getString("cid");


            this.account = jsonObject.getString("account");

            if (jsonObject.getString("code").length() != 0) {
                this.code = jsonObject.getInt("code");
            }

            this.cause = jsonObject.getString("cause");


            this.duration = jsonObject.getString("usage");

            if (duration.isEmpty()){
                duration = "0";
            }

            if (jsonObject.getString("costs").length() != 0) {
                this.costs = jsonObject.getDouble("costs");
            }
            this.source = jsonObject.getString("source");

            this.destination = jsonObject.getString("destination").split(" -", 0)[0];
            //System.out.println(destination);

            this.package_name = jsonObject.getString("package");

//            if (jsonObject.getString("package_costs").length() != 0) {
//                this.package_costs = jsonObject.getDouble("package_costs");
//            }

            if (jsonObject.getString("network_costs").length() != 0) {
                this.network_costs = jsonObject.getDouble("network_costs");
            }

            if (jsonObject.getString("service_costs").length() != 0) {
                this.service_costs = jsonObject.getDouble("service_costs");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getTargetNumber() {
        return targetNumber;
    }

    public String getDestination() {
        return destination;
    }

    public String getDuration(){
        return duration;
    }

    public int getCode(){
        return code;
    }

    public String getDir(){
        return dir;
    }

    public String getDateTimeString() {
        return dateTimeString;
    }
}