package com.example.speakup.models;

import org.json.JSONException;
import org.json.JSONObject;

public class Sms {

    private String targetNumber;

    private int usage;

    private double network_costs;

    private double service_costs;

    public Sms(JSONObject jsonObject) {
        try {
            targetNumber = jsonObject.getString("to");

            usage = jsonObject.getInt("usage");

            if (jsonObject.getString("network_costs").length() != 0) {
                this.network_costs = jsonObject.getDouble("network_costs");
            }

            if (jsonObject.getString("service_costs").length() != 0) {
                this.service_costs = jsonObject.getDouble("service_costs");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getUsage() {
        return usage;
    }

    public String getTargetNumber() {
        return targetNumber;
    }
}
