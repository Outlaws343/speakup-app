package com.example.speakup.datamanager;


import com.example.speakup.fragments.SettingsFragment;
import com.example.speakup.models.Data;
import com.example.speakup.models.Sms;
import com.example.speakup.models.Voice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class DataManager {

    private SettingsFragment settingsFragment;

    private ArrayList<Data> listOfData;
    private ArrayList<Sms> listOfSms;
    private ArrayList<Voice> listOfVoice;

    private Voice totalVoice;
    private Sms totalSms;
    private Data totalData;

    private double voiceLimit = 0.0;
    private double smsLimit = 0.0;
    private double dataLimit = 0.0;

    public DataManager() {
        listOfData = new ArrayList<>();
        listOfSms = new ArrayList<>();
        listOfVoice = new ArrayList<>();
    }

    public void loadJSONOBJECTS(String cvs) {

        String jsonString = speakupCSVtoJSON(cvs);

        File f = new File(android.os.Environment.getExternalStorageDirectory(), "json.json");
        PrintWriter out;
        try {
            out = new PrintWriter(f);
            out.write(jsonString);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // clear current lists
        listOfData.clear();
        listOfSms.clear();
        listOfVoice.clear();


        try {
            JSONArray jsonArray = new JSONArray(jsonString);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String type = jsonObject.getString("type");
                switch (type) {
                    case "Data":
                        Data d = new Data(jsonObject);
                        listOfData.add(d);
                        break;
                    case "Voice":
                        Voice v = new Voice(jsonObject);
                        listOfVoice.add(v);
                        break;
                    case "SMS":
                        Sms s = new Sms(jsonObject);
                        listOfSms.add(s);
                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Voice> getListOfVoice() {
        return listOfVoice;
    }

    public ArrayList<Sms> getListOfSms() {
        return listOfSms;
    }

    private String speakupCSVtoJSON(String input) {

        // fix input

        // the input comes in a String with start value " and last value "
        // to remove "" from the String(to prevent """ to happen)
        StringBuilder b = new StringBuilder();
        b.append(input);
        b.deleteCharAt(0);

        b.deleteCharAt(b.length() - 1);
        input = b.toString();


        //Split on \ to get headers and values
        //todo: smarter remove \n
        String[] lines = input.split("\\\\");

        // The first line should contain the headers.
        String[] headers = lines[0].split(",", -1);


        // somehow the first header seems to be broken?
        //todo better fix
        headers[0] = "cid";


        // convert to json below

        // make the headers in json format
        for (int i = 0; i < headers.length; i++) {
            headers[i] = "\"" + headers[i] + "\":";
        }


        StringBuilder builder = new StringBuilder();
        builder.append('[');

        // start at i = 1 because lines[0] are the headers
        for (int i = 1; i < lines.length; i++) {
            builder.append('{');

            String[] values = lines[i].split(",", -1);

            // remove n of the first value.
            //todo: smarter remove \n
            values[0] = deleteFirstChar(values[0]);

            for (int j = 0; j < headers.length; j++) {

                String jsonvalue = headers[j] + "\"" + values[j] + "\"";


                if (j != headers.length - 1) {
                    jsonvalue += ',';
                }
                builder.append(jsonvalue);
            }

            if (i != lines.length - 1) {
                builder.append('}');
                builder.append(',');

            } else {
                builder.append('}');
            }

        }

        builder.append(']');

        return builder.toString();
    }

    private String deleteFirstChar(String value) {
        StringBuilder v = new StringBuilder();
        v.append(value);
        v.deleteCharAt(0);
        value = v.toString();
        return value;
    }

    public ArrayList<Data> getListOfData() {
        return listOfData;
    }

    public int getTotalDataUsage() {
        int usage = 0;
        for (Data d :listOfData) {
            usage += d.getUsage();
        }
        return usage;
    }

    public double getVoiceLimit() {
        return voiceLimit;
    }

    public double getSmsLimit() {
        return smsLimit;
    }

    public double getDataLimit() {
        return dataLimit;
    }

    public void setVoiceLimit(double voiceLimit) {
        this.voiceLimit = voiceLimit;
    }

    public void setSmsLimit(double smsLimit) {
        this.smsLimit = smsLimit;
    }

    public void setDataLimit(double dataLimit) {
        this.dataLimit = dataLimit;
    }
}