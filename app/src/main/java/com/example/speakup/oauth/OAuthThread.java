package com.example.speakup.oauth;

public class OAuthThread extends Thread {

    private OAuth2Client client;

    private String csv;

    public OAuthThread(OAuth2Client client){
        this.client = client;
    }


    public void run(){
       Token token = client.getAccessToken();

        csv = token.getResource(client, token, "https://api.dev.hub.speakup.nl/saxion-persistent/mock-cdr/user/4");

    }

    public String getCsv() {
        return csv;
    }
}
