package com.example.speakup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NoWifiActivity extends AppCompatActivity {

    Button button ;
//    public static DataManager DataManager = new DataManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        button = findViewById(R.id.tryAgainButton);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_wifi);
    }


    public void probeerOpnieuw (View view){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

}
