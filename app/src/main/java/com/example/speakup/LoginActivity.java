package com.example.speakup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.speakup.datamanager.DataManager;
import com.example.speakup.oauth.OAuth2Client;
import com.example.speakup.oauth.OAuthThread;

public class LoginActivity extends AppCompatActivity {

    public static DataManager DataManager = new DataManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
    }
    

    public void onClickLogin(View view){

        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }

        if (haveConnectedWifi == true){
            Intent intent = new Intent(this, MenuActivity.class);
            startActivity(intent);
            login();
        } else if (haveConnectedMobile == true){
            Intent intent = new Intent(this, MenuActivity.class);
            startActivity(intent);
            login();
        }
        else {
            Intent intent = new Intent(this, NoWifiActivity.class);
            startActivity(intent);
        }

    }


    public void login(){
        String username = "saxion-persistent-user1";
        String password = "hoDIoNOWiD";
        String app_id = "saxion-persistent-team1";
        String app_secret = "8932ccd5-2633-4d63-8212-136697415948";


        String site = "https://account.dev.hub.speakup.nl/auth/realms/saxion-persistent/protocol/openid-connect/token";

        OAuth2Client client = new OAuth2Client(username, password, app_id, app_secret, site);

        OAuthThread thread = new OAuthThread(client);

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String csv = thread.getCsv();

        DataManager.loadJSONOBJECTS(csv);
    }
}


