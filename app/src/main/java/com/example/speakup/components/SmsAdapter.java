package com.example.speakup.components;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.speakup.R;
import com.example.speakup.models.Sms;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class SmsAdapter extends RecyclerView.Adapter<SmsAdapter.ViewHolder> {

    private ArrayList<Sms> smsData;

    public SmsAdapter(ArrayList<Sms> smsData) {
        this.smsData = smsData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sms_history_cardview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Sms sms = smsData.get(position);

        holder.phoneNumber.setText(sms.getTargetNumber());
    }

    @Override
    public int getItemCount() {
        return smsData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView dateInput;
        private TextView timeInput;
        private TextView phoneNumber;
        private TextView inOrOutTV;

        View view;

        public ViewHolder(View itemView) {
            super(itemView);

            dateInput = itemView.findViewById(R.id.dateInput);
            timeInput = itemView.findViewById(R.id.timeInput);
            phoneNumber = itemView.findViewById(R.id.phoneNumber);
            inOrOutTV = itemView.findViewById(R.id.inOrOutTV);

            view = itemView;
        }
    }
}