package com.example.speakup.components;

import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.speakup.R;
import com.example.speakup.models.Voice;

import java.util.ArrayList;


public class VoiceAdapter extends RecyclerView.Adapter<VoiceAdapter.ViewHolder> {
    private ArrayList<Voice> voiceData;

    public VoiceAdapter(ArrayList<Voice> voiceData) {
        this.voiceData = voiceData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.voice_history_cardview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Voice call = voiceData.get(position);

        holder.phoneNumberTv.setText(call.getTargetNumber());

        holder.countryTv.setText(call.getDestination());
//        if (call.getDestination().equals("Netherlands")){
//            holder.countryTv.setVisibility(View.GONE);
//        } else {
//            holder.countryTv.setVisibility(View.VISIBLE);
//        }

        holder.durationInputTv.setText(call.getDuration());

        if (call.getCode() == 200){
            if (call.getDir().equals("in")){
                holder.InOutIm.setVisibility(View.VISIBLE);
                holder.InOutIm.setImageResource(R.drawable.ic_call_incoming);
                holder.phoneNumberTv.setTextColor(Color.GRAY);
            } else {
                holder.InOutIm.setVisibility(View.VISIBLE);
                holder.InOutIm.setImageResource(R.drawable.ic_call_outgoing);
                holder.phoneNumberTv.setTextColor(Color.GRAY);
            }
        } else {
            holder.InOutIm.setVisibility(View.INVISIBLE);
            holder.phoneNumberTv.setTextColor(Color.RED);
        }

        String[] time = call.getDateTimeString().split(" ");
        holder.dateInputTv.setText(time[0]);
        holder.timeInputTv.setText(time[1]);

    }

    @Override
    public int getItemCount() {
        return voiceData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView InOutIm;
        private TextView phoneNumberTv;
        private TextView countryTv;
        private TextView durationInputTv;
//        private TextView costInputTv;
        private TextView dateInputTv;
        private TextView timeInputTv;
        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            InOutIm = itemView.findViewById(R.id.incomingOutgoingImageview);
            phoneNumberTv = itemView.findViewById(R.id.phoneNumberTextView);
            countryTv = itemView.findViewById(R.id.countryTextView);
            durationInputTv = itemView.findViewById(R.id.durationInput);
//            costInputTv = itemView.findViewById(R.id.costInputTextView);
            dateInputTv = itemView.findViewById(R.id.dateInputTextView);
            timeInputTv = itemView.findViewById(R.id.timeInputTextView);
            view = itemView;
        }
    }
}
