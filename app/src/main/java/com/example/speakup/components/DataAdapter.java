package com.example.speakup.components;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.speakup.R;
import com.example.speakup.models.Data;
import com.example.speakup.models.Voice;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<Data> data;

    public DataAdapter(ArrayList<Data> data) {
        this.data = data;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_history_cardview, parent, false);
        return new DataAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder holder, int position) {
            holder.time.setText(data.get(position).getTime());

            double usage = round((float) data.get(position).getUsage() / 1024, 2);

            holder.tv.setText(usage + " MB");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView tv;
        TextView time;


        public ViewHolder(View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.mbs);
            time = itemView.findViewById(R.id.dateInput);
            view = itemView;
        }
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
