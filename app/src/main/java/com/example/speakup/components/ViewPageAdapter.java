package com.example.speakup.components;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.example.speakup.R;

public class ViewPageAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public ViewPageAdapter(Context context){
        this.context = context;
    }
    public int[] slider_images = {
            R.drawable.ic_contacts_white,
            R.drawable.ic_data_white,
            R.drawable.ic_calls_white
    };

    public String[] headers = {
            "Contacts",
            "Data Usage",
            "Calls"
    };

    public String[] descriptions = {
            "Get clear and simple insights in your contacts",
            "A summary of the data you used and information about incoming and outgoing calls",
            "Insights in calls you made and messages you send"
    };
    @Override
    public int getCount() {
        return headers.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slider_layout, container, false);

        ImageView imageView = view.findViewById(R.id.slider_image);
        TextView header = view.findViewById(R.id.slider_header);
        TextView description = view.findViewById(R.id.slider_desc);

        imageView.setImageResource(slider_images[position]);
        header.setText(headers[position]);
        header.setTextColor(Color.WHITE);
        description.setText(descriptions[position]);
        description.setTextColor(Color.WHITE);

        container.addView(view);

        return view;

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout)object);
    }
}
