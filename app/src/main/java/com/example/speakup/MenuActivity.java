package com.example.speakup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.speakup.fragments.SettingsFragment;
import com.example.speakup.fragments.VoiceFragment;
import com.example.speakup.fragments.ContactsFragment;
import com.example.speakup.fragments.DataFragment;
import com.example.speakup.fragments.HomeFragment;
import com.example.speakup.fragments.SmsFragment;
import com.google.android.material.navigation.NavigationView;


public class MenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Welcome");
        toolbar.setSubtitle("Speakup 2019");

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.Home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                break;
            case R.id.Calls:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new VoiceFragment()).commit();
                break;
            case R.id.SMS:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SmsFragment()).commit();
                break;
            case R.id.Data:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DataFragment()).commit();
                break;
            case R.id.Settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SettingsFragment()).commit();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
